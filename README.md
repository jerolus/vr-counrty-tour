# VR Country Tour - India

This is one of my projects for [Udacity Google VR Nanodegree Program](https://eu.udacity.com/course/vr-developer-nanodegree--nd017). <br>
With this game for VR you can learn about India. You can select different 360 video guides edited by me and visit the most turistic places in India. <br>
[Link to video](https://youtu.be/LeLicZWaujg)

## Setup

- [Unity 2018.1.1f1](https://unity3d.com/es/get-unity/download/archive)
- [GVR SDK for Unity v1.170.0](https://github.com/googlevr/gvr-unity-sdk)

## Contact

Name: Jesús María Parral Puebla <br>
Gmail: jesusmaria31@gmail.com <br>
Linkedin: [Profile](https://www.linkedin.com/in/jesusmariaparralpuebla/) <br>
YouTube: [Channel](https://www.youtube.com/channel/UC69skMwVNJglUjmYYsMLivw)
