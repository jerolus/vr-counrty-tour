﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using DG.Tweening;

public class StoryTellerController : MonoBehaviour 
{
	public List<VideoClip> videoClips;
	public VideoPlayer videoPlayer;
	public AudioSource audioSource;
	public GameObject player;

	public void PlayVideoClip(VideoClip videoClip)
	{
		player.transform.DOMove(videoPlayer.gameObject.transform.position, 0f);
		videoPlayer.clip = videoClip;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
		videoPlayer.SetTargetAudioSource(0, audioSource);
		StartCoroutine(PlayVideoDelay(0.1f));
	}

	/*
	public void PlayVideoClip(string videoURL)
	{
		player.transform.DOMove(videoPlayer.gameObject.transform.position, 0f);
		videoPlayer.url = videoURL;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
		videoPlayer.SetTargetAudioSource(0, audioSource);
		StartCoroutine(PlayVideoDelay(0.1f));
	}
	*/

	private IEnumerator PlayVideoDelay(float delay)
	{
		yield return new WaitForSeconds(delay);
		videoPlayer.Play();
	}

	public void OnClickHome()
	{
		player.transform.DOMove(Vector3.zero, 0f);
	}
}
