﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputRaycast : MonoBehaviour 
{
	private bool m_isEnter = false;
	private float m_timeEnter = 0.8f;
	public UnityEvent eventToDo;

	private void Update()
	{
		if (m_isEnter)
		{
			m_timeEnter -= Time.deltaTime;
		}

		if (m_timeEnter <= 0f)
		{
			eventToDo.Invoke();
			OnPointerExit();
		}
	}

	public void OnPointerEnter()
	{
		m_isEnter = true;
		Debug.Log("enter");
	}

	public void OnPointerExit()
	{
		Debug.Log("exit");
		m_isEnter = false;
		m_timeEnter = 1.6f;
	}
}
